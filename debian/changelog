golang-github-geertjohan-go.incremental (1.0.0-2+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 22 Jan 2025 14:22:23 +0100

golang-github-geertjohan-go.incremental (1.0.0-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 10:42:54 +0000

golang-github-geertjohan-go.incremental (1.0.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Wrap long lines in changelog entries: 0.0~git20161212.0.1172aab-2.
  * Update standards version to 4.5.0, no changes needed.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Apply multi-arch hints. + golang-github-geertjohan-go.incremental-dev: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 28 Nov 2022 23:53:42 +0000

golang-github-geertjohan-go.incremental (1.0.0-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 01:19:16 +0000

golang-github-geertjohan-go.incremental (1.0.0-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 1.0.0
  * Update debian/watch with a correct filenamemangle
  * Apply "cme fix dpkg" fixes:
    - Change Priority from "extra" to "optional"
    - Update debhelper dependency to "Build-Depends: debhelper-compat (= 12)"
    - Add "Testsuite: autopkgtest-pkg-go"
    - Use secure https protocol for Format field in debian/copyright
    - Bump Standards-Version to 4.4.1 (no change)
  * Correct license text of BSD-2-clause debian/copyright
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Add "Rules-Requires-Root: no" to debian/control
  * Add myself to the list of Uploaders
  * Simplify override_dh_auto_install with --no-binaries
  * debian/gbp.conf: Set debian-branch to debian/sid for DEP-14 conformance

 -- Anthony Fok <foka@debian.org>  Tue, 03 Dec 2019 00:55:28 -0700

golang-github-geertjohan-go.incremental (0.0~git20161212.0.1172aab-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:53:05 +0000

golang-github-geertjohan-go.incremental (0.0~git20161212.0.1172aab-2) unstable; urgency=medium

  * Replace golang-go with golang-any in Build-Depends, remove golang-go from
    Depends

 -- Konstantinos Margaritis <markos@debian.org>  Tue, 08 Aug 2017 20:10:08 +0300

golang-github-geertjohan-go.incremental (0.0~git20161212.0.1172aab-1) unstable; urgency=medium

  * Initial release (Closes: #847404)

 -- Liang Yan <liangy@hpe.com>  Mon, 21 Nov 2016 14:12:51 -0700
